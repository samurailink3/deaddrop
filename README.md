DeadDrop!
=========

## The Easy Self-Hosted File Uploader

### What?

A tiny [Sinatra](http://www.sinatrarb.com/) app designed to allow brain-dead simple uploads to either the local filesystem or Amazon S3.

### Why?

I needed a simple way for family members to send me large file attachments and didn't want to use a third-party uploader or service. This is a stupid-simple drag and drop solution (with a form for IE users) that can upload to the local filesystem or Amazon S3. And it was another excuse to get familiar with Sinatra.

### Props!

Major credit goes to [blueimp](https://github.com/blueimp/) for his ultra-rad [jQuery File Upload](https://github.com/blueimp/jQuery-File-Upload) plugin. Without whom, no drag and drop support would be possible.

### Limitations

By default, there are no restrictions on filesize, type, or number of files. If you would like to restrict these things in any way check out [blueimp's documentation here](https://github.com/blueimp/jQuery-File-Upload/wiki/Options).

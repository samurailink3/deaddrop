require 'sinatra'
require 'haml'
require 'aws/s3'
if File.exist?("./config.rb")
  require './config.rb'
end

set :haml, :format => :html5

get '/' do
  haml :index
end

get '/upload' do
  haml :upload
end

post '/upload' do
  @uploaded =  params['files']
  @uploaded.each do |f|
    if ENV['LOCAL_UPLOAD'] == 'true'
      path = File.join("uploaded", Time.now.to_f.to_s + f[:filename])
      File.open(path, "wb") { |n| n.write(f[:tempfile].read) }
    end
    if ENV['AMAZON_S3_UPLOAD'] == 'true'
      upload(f[:filename], f[:tempfile])
    end
  end
  haml :postupload
end

helpers do
    def upload(filename, file)
    bucket = ENV['BUCKET_NAME']
    AWS::S3::Base.establish_connection!(
      :access_key_id     => ENV['ACCESS_KEY_ID'],
      :secret_access_key => ENV['SECRET_ACCESS_KEY']
    )
    AWS::S3::S3Object.store(
      filename,
      open(file.path),
      bucket
    )
    return filename
  end
end
